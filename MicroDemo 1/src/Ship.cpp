#include "Ship.h"
#include "Constantes.h"

Ship::Ship (int longitud) : _longitud(longitud), _contador(longitud), _hundido(false) {
    _sentido = SENTIDO_ESTE;
    _filaInicio = -1;
    _columnaInicio = -1;
}

Ship::Ship () : _hundido(false) {
    _longitud = 0;
    _contador = 0;
    _sentido = SENTIDO_ESTE;
}

Ship::~Ship () {
}

int Ship::getLongitud () const {
    return _longitud;
}

int Ship::getContador () const {
    return _contador;
}

bool Ship::getHundido () const {
    return _hundido;
}

short Ship::getSentido () const {
    return _sentido;
}

int Ship::getFilaInicio () const {
    return _filaInicio;
}

int Ship::getColumnaInicio () const {
    return _columnaInicio;
}

void Ship::setLongitud (int longitud) {
    _longitud = longitud;
    _contador = longitud;
}

void Ship::setHundido (bool hundido) {
    _hundido = hundido;
}

void Ship::setSentido (short sentido) {
    _sentido = sentido;
}

void Ship::setFilaInicio (int filaInicio) {
    _filaInicio = filaInicio;
}

void Ship::setColumnaInicio (int columnaInicio) {
    _columnaInicio = columnaInicio;
}

void Ship::tocarBarco () {
    _contador--;

    if (_contador == 0) {
        _hundido = true;
    }
}
