#include "BattleShip.h"

BattleShip::BattleShip () {
    srand(time(NULL));

    _sceneManager = NULL;
    _framelistener = NULL;
}

BattleShip::~BattleShip () {
    delete _root;
    delete _framelistener;
}

int BattleShip::start () {
    _root = new Ogre::Root();

    if(!_root->restoreConfig()) {
        _root->showConfigDialog();
        _root->saveConfig();
    }

    Ogre::RenderWindow* window = _root->initialise(true, "BattleShip Game");
    _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);

    Ogre::Camera* camera = _sceneManager->createCamera("MainCamera");
    camera->setPosition(Ogre::Vector3(0, 5, 25));
    camera->lookAt(Ogre::Vector3(0, 0, 0));
    camera->setNearClipDistance(5);
    camera->setFarClipDistance(10000);

    Ogre::Viewport* viewport = window->addViewport(camera);
    viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    double width = viewport->getActualWidth();
    double height = viewport->getActualHeight();
    camera->setAspectRatio(width / height);

    loadResources();
    createScene();

    _framelistener = new MyFrameListener(window, camera, _sceneManager);
    _root->addFrameListener(_framelistener);

    _root->startRendering();

    return 0;
}

void BattleShip::loadResources () {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;    datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                (datastr, typestr, sectionstr);
        }
    }

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void BattleShip::createScene () {
    /* Añadidas dos luces al Root */
   Ogre::Light* light1 = _sceneManager->createLight();
   light1->setType(Ogre::Light::LT_POINT);
   light1->setPosition(-10, 100, 100);
   light1->setSpecularColour(Ogre::ColourValue::White);
   _sceneManager->getRootSceneNode()->attachObject(light1);

   Ogre::Light* light2 = _sceneManager->createLight();
   light2->setType(Ogre::Light::LT_POINT);
   light2->setPosition(10, -100, -100);
   light2->setSpecularColour(Ogre::ColourValue::White);
   _sceneManager->getRootSceneNode()->attachObject(light2);
}
