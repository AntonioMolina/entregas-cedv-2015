#include "Tablero.h"

Tablero::Tablero () {
    _tablero = new Casilla* [DIMENSION_TABLERO];

    for (int i = 0; i < DIMENSION_TABLERO; i++) {
        _tablero[i] = new Casilla[DIMENSION_TABLERO];
    }
}

Tablero::~Tablero () {
    for (int i = 0; i < DIMENSION_TABLERO; i++) {
        delete[] _tablero[i];
    }
    delete[] _tablero;
}

Ship* Tablero::getBarcos () {
    return _barcos;
}

Casilla* Tablero::getCasilla (int x, int y) {
    return &_tablero[x][y];
}

void Tablero::inicializarBarcos () {
    _barcos = new Ship[NUM_BARCOS];
    Ship* aux;

    for (int i = 0; i < NUM_BARCOS; i++) {
        aux = new Ship();
        switch (i) {
            case 0:
            case 1:
                aux->setLongitud(2);
                break;
            case 2:
            case 3:
                aux->setLongitud(3);
                break;
            case 4:
                aux->setLongitud(4);
        }

        /* SENTIDO_ESTE 0, SENTIDO_NORTE 1, SENTIDO_OESTE 2, SENTIDO_SUR 3 */
        aux->setSentido(rand() % 4);

        _barcos[i] = *aux;
    }
}

void Tablero::colocarBarcos () {
    inicializarBarcos();

    for (int i = 0; i < NUM_BARCOS; i++) {
        bool valido = false;
        Ship* aux;

        aux = &_barcos[i];

        while (!valido) {
            int x = rand() % DIMENSION_TABLERO;
            int y = rand() % DIMENSION_TABLERO;

            switch (aux->getSentido()) {
                case SENTIDO_ESTE:
                    if ((y + aux->getLongitud() - 1) < DIMENSION_TABLERO) {
                        valido = true;
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x, y + j);
                            if (cas->getBarco() != NULL) {
                                valido = false;
                            }
                            /* Se revisan las casillas de alrededor para evitar
                             * que estén juntos los barcos */
                            if (valido) {
                                if (j == 0 && y > 0) {
                                    cas = getCasilla(x, y - 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (x > 0) {
                                    cas = getCasilla(x - 1, y + j);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (x < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x + 1, y + j);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (j == (aux->getLongitud() - 1) && (y + aux->getLongitud() - 1) < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x, y + j + 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                            }
                        }
                    }
                    if (valido) {
                        aux->setFilaInicio(x);
                        aux->setColumnaInicio(y);
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x, y + j);
                            cas->setBarco(aux);
                        }
                    }
                    break;
                case SENTIDO_OESTE:
                    if ((y - aux->getLongitud() + 1) >= 0) {
                        valido = true;
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x, y - j);
                            if (cas->getBarco() != NULL) {
                                valido = false;
                            }
                            /* Se revisan las casillas de alrededor para evitar
                             * que estén juntos los barcos */
                            if (valido) {
                                if (j == 0 && y < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x, y + 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (x > 0) {
                                    cas = getCasilla(x - 1, y - j);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (x < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x + 1, y - j);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (j == (aux->getLongitud() - 1) && (y - aux->getLongitud() + 1) > 0) {
                                    cas = getCasilla(x, y - j - 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                            }
                        }
                    }
                    if (valido) {
                        aux->setFilaInicio(x);
                        aux->setColumnaInicio(y);
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x, y - j);
                            cas->setBarco(aux);
                        }
                    }
                    break;
                case SENTIDO_NORTE:
                    if ((x - aux->getLongitud() + 1) >= 0) {
                        valido = true;
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x - j, y);
                            if (cas->getBarco() != NULL) {
                                valido = false;
                            }
                            /* Se revisan las casillas de alrededor para evitar
                             * que estén juntos los barcos */
                            if (valido) {
                                if (j == 0 && x < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x + 1, y);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (y > 0) {
                                    cas = getCasilla(x - j, y - 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (y < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x - j, y + 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (j == (aux->getLongitud() - 1) && (x - aux->getLongitud() + 1) > 0) {
                                    cas = getCasilla(x - j - 1, y);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                            }
                        }
                    }
                    if (valido) {
                        aux->setFilaInicio(x);
                        aux->setColumnaInicio(y);
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x - j, y);
                            cas->setBarco(aux);
                        }
                    }
                    break;
                case SENTIDO_SUR:
                    if ((x + aux->getLongitud() - 1) < DIMENSION_TABLERO) {
                        valido = true;
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x + j, y);
                            if (cas->getBarco() != NULL) {
                                valido = false;
                            }
                            /* Se revisan las casillas de alrededor para evitar
                             * que estén juntos los barcos */
                            if (valido) {
                                if (j == 0 && x > 0) {
                                    cas = getCasilla(x - 1, y);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (y > 0) {
                                    cas = getCasilla(x + j, y - 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (y < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x + j, y + 1);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                                if (j == (aux->getLongitud() - 1) && (x + aux->getLongitud() - 1) < (DIMENSION_TABLERO - 1)) {
                                    cas = getCasilla(x + j + 1, y);
                                    if (cas->getBarco() != NULL) {
                                        valido = false;
                                    }
                                }
                            }
                        }
                    }
                    if (valido) {
                        aux->setFilaInicio(x);
                        aux->setColumnaInicio(y);
                        for (int j = 0; j < aux->getLongitud(); j++) {
                            Casilla* cas = getCasilla(x + j, y);
                            cas->setBarco(aux);
                        }
                    }
                    break;
            }
        }
    }
}

/*bool Tablero::tocarCasilla (int x, int y) {
    Casilla* cas = &_tablero[x][y];
    if (!cas->getUsada()) {
        cas->tocar();

        return true;
    } else {
        return false;
    }
}*/
