#include <stddef.h>

#include "Casilla.h"

Casilla::Casilla () {
    _usada = false;
    _barco = NULL;
}

Casilla::~Casilla () {
    if (_barco) {
        delete _barco;
    }
}

bool Casilla::getUsada () {
    return _usada;
}

Ship* Casilla::getBarco () {
    return _barco;
}

void Casilla::setUsada (bool usada) {
    _usada = usada;
}

void Casilla::setBarco (Ship* barco) {
    _barco = barco;
}

void Casilla::tocar () {
    _usada = true;

    if (_barco != NULL) {
        _barco->tocarBarco();
    }
}
