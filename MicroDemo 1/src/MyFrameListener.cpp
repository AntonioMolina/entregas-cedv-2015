#include "MyFrameListener.h"

MyFrameListener::MyFrameListener (RenderWindow* window, Camera* camera,
            SceneManager* sceneManager) : _window(window), _camera(camera),
            _sceneManager(sceneManager) {
    OIS::ParamList param;

    size_t windowHandle;
    std::ostringstream wHandleStr;

    _window->getCustomAttribute("WINDOW", &windowHandle);
    wHandleStr << windowHandle;
    param.insert(std::make_pair("WINDOW", wHandleStr.str()));

    _inputManager = OIS::InputManager::createInputSystem(param);

    _keyboard = static_cast<OIS::Keyboard*>(_inputManager->createInputObject(OIS::OISKeyboard, true));
    _mouse = static_cast<OIS::Mouse*>(_inputManager->createInputObject(OIS::OISMouse, true));
    _mouse->getMouseState().width = _window->getWidth();
    _mouse->getMouseState().height = _window->getHeight();

    _keyboard->setEventCallback(this);
    _mouse->setEventCallback(this);

    _raySceneQuery = _sceneManager->createRayQuery(Ray());
    _selectedNode = NULL;

    _estado = ESTADO_MENU;
    _dificultadIA = IA_FACIL;

    createRecordsFile();
    setCEGUI();

    createBackGround();
    createGUIMenu();
}

MyFrameListener::~MyFrameListener () {
    _inputManager->destroyInputObject(_keyboard);
    _inputManager->destroyInputObject(_mouse);
    OIS::InputManager::destroyInputSystem(_inputManager);
    _sceneManager->destroyQuery(_raySceneQuery);
}

Ray MyFrameListener::setRayQuery (int posx, int posy, uint32 mask) {
    Ray rayMouse = _camera->getCameraToViewportRay
            (posx/float(_window->getWidth()), posy/float(_window->getHeight()));

    _raySceneQuery->setRay(rayMouse);
    _raySceneQuery->setSortByDistance(true);
    _raySceneQuery->setQueryMask(mask);

    return (rayMouse);
}

bool MyFrameListener::frameStarted (const FrameEvent& evt) {
    _timeSinceLastFrame = evt.timeSinceLastFrame;
    bool mbleft;

    CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);

    /* Captura de eventos */
    _keyboard->capture();
    _mouse->capture();

    /* Posición del ratón */
    float posx = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition().d_x;//_mouse->getMouseState().X.abs;
    float posy = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition().d_y;//_mouse->getMouseState().Y.abs;

    mbleft = _mouse->getMouseState().buttonDown(OIS::MB_Left);

    switch (_estado) {
    case ESTADO_CARGA:
        createGUIGame();
        initGame();
        _estado = ESTADO_JUEGO;
        _movimientos = 0;
        break;
    case ESTADO_JUEGO:
        if (_turno == TURNO_JUGADOR) {
            uint32 mask = CUBO_TABLERO;

            if (mbleft) {
                if (DEBUG_MODE) {
                    cout << "Botón Izquierdo" << endl;
                }
                mask = CUBO_TABLERO;
            }

            if (_selectedNode != NULL) {
                _selectedNode->showBoundingBox(false);
                _selectedNode = NULL;
            }

            Ray r = setRayQuery(posx, posy, mask);
            UNUSED_VARIABLE(r);
            RaySceneQueryResult &result = _raySceneQuery->execute();
            RaySceneQueryResult::iterator it;
            it = result.begin();

            if (it != result.end()) {
                if (mbleft) {
                    Ogre::Node *nodeaux = it->movable->getParentNode();
                    /* Se ignoran los nodos tableros */
                    if (nodeaux->getName() != "TableroJugador" && nodeaux->getName() != "TableroIA") {
                        if (DEBUG_MODE) {
                            cout << nodeaux->getName() << endl;
                        }

                        _selectedNode = it->movable->getParentSceneNode();

                        string nombreNode = _selectedNode->getName();
                        std::vector<string> nombreNodeSplit = split(nombreNode, '_');

                        /* Comprobar que nombreNodeSplit[0] es Col y que realmente se tiene de longitud 3 */
                        if (nombreNodeSplit.size() > 2 && strcmp(nombreNodeSplit[0].c_str(), "Col") == 0) {
                            /* Se obtienen las posiciones Fila y Columna de la casilla en el tablero
                             * Se obtiene tambien el puntero a la casilla correspondiente */
                            int posCasillaFila = atoi(nombreNodeSplit[1].c_str()) - 1;
                            int posCasillaColumna = atoi(nombreNodeSplit[2].c_str()) - 1;

                            Casilla* cas = _tableroIA->getCasilla(posCasillaFila, posCasillaColumna);
                            if (!cas->getUsada()) {
                                cas->tocar();
                                _movimientos++;

                                /* Se pintan las cruces blancas si se ha tocado o no */
                                Ship* barco = cas->getBarco();

                                stringstream nombreNodoCol;
                                nombreNodoCol.str("");
                                nombreNodoCol << "Col_" << (posCasillaFila + 1) << "_" << (posCasillaColumna + 1);
                                SceneNode *aux = _sceneManager->getSceneNode(nombreNodoCol.str());

                                stringstream sauxnode;
                                sauxnode.str("");
                                sauxnode << "Cruz_" << posCasillaFila << "_" << posCasillaColumna;
                                SceneNode *nodoCruzAux = _sceneManager->createSceneNode(sauxnode.str());

                                Entity *entCruzAux;
                                if (barco != NULL) {
                                    entCruzAux = _sceneManager->createEntity("CruzRoja.mesh");
                                } else {
                                    entCruzAux = _sceneManager->createEntity("CruzBlanca.mesh");
                                }
                                nodoCruzAux->attachObject(entCruzAux);

                                aux->addChild(nodoCruzAux);
                                float f = - 4.6 + posCasillaFila;
                                float c = 0;
                                float front = 0;
                                if (posCasillaColumna < 5) {
                                    c = - 4.6 + posCasillaColumna;
                                } else {
                                    c = posCasillaColumna - 4.5 + (posCasillaColumna * 0.01);
                                }
                                if (posCasillaFila > 7) {
                                    front = 0.5;
                                }
                                nodoCruzAux->translate(Vector3(c, front, f));

                                /* Se cambia el turno para la IA */
                                _turno = TURNO_IA;
                            }

                            if (comprobarGanar(TURNO_JUGADOR)) {
                                _ganador = GANADOR_JUGADOR;
                                _estado = ESTADO_RESULTADO;
                                saveRecords();
                                deleteGUIGame();
                                createGUIResultado();
                            }
                        }
                    }

                    _selectedNode = it->movable->getParentSceneNode();
                    if (DEBUG_MODE) {
                        _selectedNode->showBoundingBox(true);
                    }
                }
            }
        } else if (_turno == TURNO_IA){
            ejecutarTurnoIA();

            if (comprobarGanar(TURNO_IA)) {
                _ganador = GANADOR_IA;
                _estado = ESTADO_RESULTADO;
                deleteGUIGame();
                createGUIResultado();
            }
            _turno = TURNO_JUGADOR;
        }

        break;
    case ESTADO_SALIR:
        return false;
    }

    return true;
}

bool MyFrameListener::keyPressed (const OIS::KeyEvent& evt) {
    switch (_estado) {
        case ESTADO_MENU:
            if (evt.key == OIS::KC_ESCAPE) {
                _estado = ESTADO_SALIR;
                deleteBackGround();
                deleteGUIMenu();
                cout << "Salgo" << endl;
            } else if (evt.key == OIS::KC_SPACE) {
                _estado = ESTADO_CARGA;
                deleteBackGround();
                deleteGUIMenu();
                cout << "Juego" << endl;
            } else if (evt.key == OIS::KC_R) {
                _estado = ESTADO_RECORDS;
                deleteGUIMenu();
                createGUIRecords();
                cout << "Records" << endl;
            } else if (evt.key == OIS::KC_C) {
                _estado = ESTADO_CREDITOS;
                deleteGUIMenu();
                createGUICreditos();
                cout << "Creditos" << endl;
            } else if (evt.key == OIS::KC_H) {
                _estado = ESTADO_HOWTO;
                deleteGUIMenu();
                createGUIHowToPlay();
                cout << "Como Jugar" << endl;
            } else if (evt.key == OIS::KC_I) {
                _estado = ESTADO_SELEC_IA;
                deleteGUIMenu();
                createGUIDificultad();
                cout << "Dificultad" << endl;
            }
            break;
        case ESTADO_JUEGO:
            if (evt.key == OIS::KC_ESCAPE) {
                _estado = ESTADO_MENU;
                limpiarEscena();
                deleteGUIGame();
                createBackGround();
                createGUIMenu();
                cout << "Menu" << endl;
            } else if (evt.key == OIS::KC_P) {
                _estado = ESTADO_PAUSA;
                deleteGUIGame();
                createGUIPause();
                cout << "Pausa" << endl;
            }
            break;
        case ESTADO_PAUSA:
            if (evt.key == OIS::KC_SPACE || evt.key == OIS::KC_P) {
                _estado = ESTADO_JUEGO;
                deleteGUIPause();
                createGUIGame();
                cout << "Juego" << endl;
            } else if (evt.key == OIS::KC_ESCAPE) {
                _estado = ESTADO_MENU;
                limpiarEscena();
                deleteGUIPause();
                createBackGround();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
        case ESTADO_RESULTADO:
            if (evt.key == OIS::KC_SPACE) {
                _estado = ESTADO_CARGA;
                limpiarEscena();
                deleteGUIResultado();
                cout << "Juego" << endl;
            } else if (evt.key == OIS::KC_ESCAPE) {
                _estado = ESTADO_MENU;
                limpiarEscena();
                deleteGUIResultado();
                createBackGround();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
        case ESTADO_RECORDS:
            if (evt.key == OIS::KC_ESCAPE || evt.key == OIS::KC_R) {
                _estado = ESTADO_MENU;
                deleteGUIRecords();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
        case ESTADO_CREDITOS:
            if (evt.key == OIS::KC_ESCAPE || evt.key == OIS::KC_C) {
                _estado = ESTADO_MENU;
                deleteGUICreditos();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
        case ESTADO_HOWTO:
            if (evt.key == OIS::KC_ESCAPE || evt.key == OIS::KC_H) {
                _estado = ESTADO_MENU;
                deleteGUIHowToPlay();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
        case ESTADO_SELEC_IA:
            if (evt.key == OIS::KC_ESCAPE) {
                _estado = ESTADO_MENU;
                deleteGUIDificultad();
                createGUIMenu();
                cout << "Menu" << endl;
            }
            break;
    }

    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
    CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

    return true;
}

bool MyFrameListener::keyReleased (const OIS::KeyEvent& evt) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));
    return true;
}

bool MyFrameListener::mouseMoved (const OIS::MouseEvent& evt) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    return true;
}

bool MyFrameListener::mousePressed (const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
    return true;
}

bool MyFrameListener::mouseReleased (const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

CEGUI::MouseButton MyFrameListener::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}

/* Métodos para la gestión de CEGUI */
/* Menu */
bool MyFrameListener::startGame (const CEGUI::EventArgs &e) {
    _estado = ESTADO_CARGA;
    deleteBackGround();
    deleteGUIMenu();
    cout << "Juego" << endl;
    return true;
}

bool MyFrameListener::exitGame (const CEGUI::EventArgs &e) {
    _estado = ESTADO_SALIR;
    deleteBackGround();
    deleteGUIMenu();
    cout << "Salgo" << endl;
    return true;
}

bool MyFrameListener::recordsGame (const CEGUI::EventArgs &e) {
    _estado = ESTADO_RECORDS;
    deleteGUIMenu();
    createGUIRecords();
    cout << "Records" << endl;
    return true;
}

bool MyFrameListener::howtoGame (const CEGUI::EventArgs &e) {
    _estado = ESTADO_HOWTO;
    deleteGUIMenu();
    createGUIHowToPlay();
    cout << "Como Jugar" << endl;
    return true;
}

bool MyFrameListener::creditosGame (const CEGUI::EventArgs &e) {
    _estado = ESTADO_CREDITOS;
    deleteGUIMenu();
    createGUICreditos();
    cout << "Creditos" << endl;
    return true;
}

bool MyFrameListener::selectDificultad (const CEGUI::EventArgs &e) {
    _estado = ESTADO_SELEC_IA;
    deleteGUIMenu();
    createGUIDificultad();
    cout << "Dificultad" << endl;
    return true;
}

/* Juego */
bool MyFrameListener::pauseMatch (const CEGUI::EventArgs &e) {
    _estado = ESTADO_PAUSA;
    deleteGUIGame();
    createGUIPause();
    cout << "Pausa" << endl;
    return true;
}

bool MyFrameListener::exitMatch (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    limpiarEscena();
    deleteGUIGame();
    createBackGround();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

bool MyFrameListener::ocultarBarcos (const CEGUI::EventArgs &e) {
    _sceneManager->getSceneNode("Barco_0")->setVisible(false);
    _sceneManager->getSceneNode("Barco_1")->setVisible(false);
    _sceneManager->getSceneNode("Barco_2")->setVisible(false);
    _sceneManager->getSceneNode("Barco_3")->setVisible(false);
    _sceneManager->getSceneNode("Barco_4")->setVisible(false);

    return true;
}

bool MyFrameListener::mostrarBarcos (const CEGUI::EventArgs &e) {
    _sceneManager->getSceneNode("Barco_0")->setVisible(true);
    _sceneManager->getSceneNode("Barco_1")->setVisible(true);
    _sceneManager->getSceneNode("Barco_2")->setVisible(true);
    _sceneManager->getSceneNode("Barco_3")->setVisible(true);
    _sceneManager->getSceneNode("Barco_4")->setVisible(true);

    return true;
}

/* Pausa */
bool MyFrameListener::resumeMatch (const CEGUI::EventArgs &e) {
    _estado = ESTADO_JUEGO;
    deleteGUIPause();
    createGUIGame();
    cout << "Juego" << endl;
    return true;
}

bool MyFrameListener::finishMatch (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    limpiarEscena();
    deleteGUIPause();
    createBackGround();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

/* Resultados */
bool MyFrameListener::gameAgain (const CEGUI::EventArgs &e) {
    _estado = ESTADO_CARGA;
    limpiarEscena();
    deleteGUIResultado();
    cout << "Juego" << endl;
    return true;
}

bool MyFrameListener::notPlayAgain (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    limpiarEscena();
    deleteGUIResultado();
    createBackGround();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

/* Como jugar */
bool MyFrameListener::howToMenu (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    deleteGUIHowToPlay();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

/* Records */
bool MyFrameListener::exitRecords (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    deleteGUIRecords();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

/* Créditos */
bool MyFrameListener::exitCreditos (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    deleteGUICreditos();
    createGUIMenu();
    cout << "Menu" << endl;
    return true;
}

/* Dificultad */
bool MyFrameListener::selecFacil (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    _dificultadIA = IA_FACIL;
    deleteGUIDificultad();
    createGUIMenu();
    return true;
}

bool MyFrameListener::selecNormal (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    _dificultadIA = IA_NORMAL;
    deleteGUIDificultad();
    createGUIMenu();
    return true;
}

bool MyFrameListener::selecImposible (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    _dificultadIA = IA_EXPERTO;
    deleteGUIDificultad();
    createGUIMenu();
    return true;
}

bool MyFrameListener::exitDificultad (const CEGUI::EventArgs &e) {
    _estado = ESTADO_MENU;
    deleteGUIDificultad();
    createGUIMenu();
    return true;
}

/* Creación y borrado del fondo */
/* Código Obtenido de http://www.ogre3d.org/tikiwiki/Displaying+2D+Backgrounds */
void MyFrameListener::createBackGround () {
    MaterialPtr material = MaterialManager::getSingleton().create("Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("MenuBackground.jpg");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    Rectangle2D* rect = new Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("Background");

    rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

    AxisAlignedBox aabInf;
    aabInf.setInfinite();
    rect->setBoundingBox(aabInf);

    SceneNode* node = _sceneManager->getRootSceneNode()->createChildSceneNode("Background");
    node->attachObject(rect);
}

void MyFrameListener::deleteBackGround () {
    _sceneManager->destroySceneNode("Background");
}

/* Métodos para creación de GUI para los distintos estados */
void MyFrameListener::setCEGUI () {
    _renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");

    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","BattleShip");

    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void MyFrameListener::createGUIMenu () {
    /* Nombre Juego */
    CEGUI::Window* nombreJuego = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "NombreJuego");
    nombreJuego->setText("[colour='FF000000'][font='Funkrocker-Titulo']BattleShip");
    nombreJuego->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.1,0)));
    nombreJuego->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.3/2,0),CEGUI::UDim(0.05,0)));
    _sheet->addChild(nombreJuego);

    /* Botón Jugar */
    CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayButton");
    playButton->setText("[font='Funkrocker-12']Jugar");
    playButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.25,0)));
    playButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::startGame, this));
    _sheet->addChild(playButton);

    /* Botón Dificultad */
    CEGUI::Window* difButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "DificultadButton");
    difButton->setText("[font='Funkrocker-12']Dificultad");
    difButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    difButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.325,0)));
    difButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::selectDificultad, this));
    _sheet->addChild(difButton);

    /* Botón Records */
    CEGUI::Window* recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "RecordsButton");
    recordsButton->setText("[font='Funkrocker-12']Records");
    recordsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));
    recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::recordsGame, this));
    _sheet->addChild(recordsButton);

    /* Botón Como Jugar */
    CEGUI::Window* howToButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "HowToButton");
    howToButton->setText("[font='Funkrocker-12']Como Jugar");
    howToButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    howToButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.475,0)));
    howToButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::howtoGame, this));
    _sheet->addChild(howToButton);

    /* Botón Creditos */
    CEGUI::Window* creditosButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "CreditosButton");
    creditosButton->setText("[font='Funkrocker-12']Creditos");
    creditosButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditosButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.55,0)));
    creditosButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::creditosGame, this));
    _sheet->addChild(creditosButton);

    /* Botón Salir */
    CEGUI::Window* salirButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "SalirButton");
    salirButton->setText("[font='Funkrocker-12']Salir");
    salirButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    salirButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.625,0)));
    salirButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::exitGame, this));
    _sheet->addChild(salirButton);
}

void MyFrameListener::createGUIGame () {
    /* Botón mostrar / ocultar barcos */
    CEGUI::Window* mostrarButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "MostrarButton");
    mostrarButton->setText("[font='Funkrocker-12']Mostrar Barcos");
    mostrarButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    mostrarButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.5,0)));
    mostrarButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::mostrarBarcos, this));
    _sheet->addChild(mostrarButton);

    CEGUI::Window* ocultarButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "OcultarButton");
    ocultarButton->setText("[font='Funkrocker-12']Ocultar Barcos");
    ocultarButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    ocultarButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.575,0)));
    ocultarButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::ocultarBarcos, this));
    _sheet->addChild(ocultarButton);

    /* Botón Parar */
    CEGUI::Window* pararButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PararButton");
    pararButton->setText("[font='Funkrocker-12']Pausa");
    pararButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    pararButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.65,0)));
    pararButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::pauseMatch, this));
    _sheet->addChild(pararButton);

    /* Botón Salir */
    CEGUI::Window* salirButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "SalirButton");
    salirButton->setText("[font='Funkrocker-12']Menu");
    salirButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    salirButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.725,0)));
    salirButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::exitMatch, this));
    _sheet->addChild(salirButton);
}

void MyFrameListener::createGUIPause () {
    /* Botón Reanudar */
    CEGUI::Window* reanudarButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ReanudarButton");
    reanudarButton->setText("[font='Funkrocker-12']Reanudar");
    reanudarButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    reanudarButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.25,0)));
    reanudarButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::resumeMatch, this));
    _sheet->addChild(reanudarButton);

    /* Botón Salir */
    CEGUI::Window* salirButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "SalirButton");
    salirButton->setText("[font='Funkrocker-12']Menu");
    salirButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    salirButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.325,0)));
    salirButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::finishMatch, this));
    _sheet->addChild(salirButton);
}

void MyFrameListener::createGUIResultado () {
    /* Aviso si has perdido o si has ganado */
    CEGUI::Window* resultadoLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Resultado");
    if (_ganador == GANADOR_JUGADOR) {
        resultadoLabel->setText("[colour='FF000000'][font='Funkrocker-25']HAS GANADO!");
    } else {
        resultadoLabel->setText("[colour='FF000000'][font='Funkrocker-25']HAS PERDIDO!");
    }
    resultadoLabel->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.3,0)));
    resultadoLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15,0),CEGUI::UDim(0.05,0)));
    _sheet->addChild(resultadoLabel);

    /* Botón Jugar de Nuevo */
    CEGUI::Window* playAgainButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayAgainButton");
    playAgainButton->setText("[font='Funkrocker-12']Jugar de Nuevo");
    playAgainButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playAgainButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.25,0)));
    playAgainButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::gameAgain, this));
    _sheet->addChild(playAgainButton);

    /* Botón Salir */
    CEGUI::Window* notPlayAgainButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "NotPlayAgainButton");
    notPlayAgainButton->setText("[font='Funkrocker-12']Menu");
    notPlayAgainButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    notPlayAgainButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.325,0)));
    notPlayAgainButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::notPlayAgain, this));
    _sheet->addChild(notPlayAgainButton);
}

void MyFrameListener::createGUIRecords () {
    /* Tabla records */
    stringstream rcdsString;
    rcdsString.str("");
    rcdsString << "[colour='FF000000'][font='Funkrocker-25']Puesto    Movimientos\n";

    string s = createStringRecords();
    std::vector<string> rcds = split(s, '\n');

    for (uint i = 1; i <= rcds.size(); i++) {
        string aux = rcds[i - 1];
        std::vector<string> v = split(aux, ' ');
        rcdsString << v[0] << "              " << v[1] << "\n";
    }

    CEGUI::Window* recordsLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabel");
    recordsLabel->setText(rcdsString.str());
    recordsLabel->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.45/2,0),CEGUI::UDim(0.15,0)));
    _sheet->addChild(recordsLabel);

    /* Botón Volver Menu */
    CEGUI::Window* menuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "MenuButton");
    menuButton->setText("[font='Funkrocker-12']Volver");
    menuButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.65,0)));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::exitRecords, this));
    _sheet->addChild(menuButton);
}

void MyFrameListener::createGUICreditos () {
    /* Botón Volver Menu */
    CEGUI::Window* menuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "MenuButton");
    menuButton->setText("[font='Funkrocker-12']Volver");
    menuButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.55,0)));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::exitCreditos, this));
    _sheet->addChild(menuButton);

    /* Texto de los autores */
    CEGUI::Window* creditos = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Creditos");
    creditos->setText("[colour='FF000000'][font='Funkrocker-25']Maldonado Burgos, Julia\nMolina Maroto, Antonio");
    creditos->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.3,0)));
    creditos->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15,0),CEGUI::UDim(0.2,0)));
    _sheet->addChild(creditos);
}

void MyFrameListener::createGUIHowToPlay () {
    /* Botón Volver Menu */
    CEGUI::Window* menuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "MenuButton");
    menuButton->setText("[font='Funkrocker-12']Volver");
    menuButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::howToMenu, this));
    _sheet->addChild(menuButton);

    /* Texto sobre como se juega */
    CEGUI::Window* howto = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "HowTo");
    howto->setText("[colour='FF000000'][font='Funkrocker-25']Los barcos se colocan de manera aleatoria. \nPodra elegir una dificultad: \nFacil, normal o imposible.\nEl primer turno siempre es aleatorio, a expcecion de \nimposible.\nDebera intentar hundir la flota del contrario \nantes que el a ti. Las cruces rojas \nindican tocado, las blancas agua.");
    howto->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.7,0)));
    howto->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.02,0)));
    _sheet->addChild(howto);
}

void MyFrameListener::createGUIDificultad () {
    /* Dificultades */
    CEGUI::Window* facilButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "FacilButton");
    facilButton->setText("[font='Funkrocker-12']Facil");
    facilButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    facilButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.2,0)));
    facilButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::selecFacil, this));
    _sheet->addChild(facilButton);

    /*CEGUI::Window* normalButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "NormalButton");
    normalButton->setText("[font='Funkrocker-12']Normal");
    normalButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    normalButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.3,0)));
    normalButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::selecNormal, this));
    _sheet->addChild(normalButton);*/

    CEGUI::Window* imposibleButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ExpertoButton");
    imposibleButton->setText("[font='Funkrocker-12']Imposible");
    imposibleButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    imposibleButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.3,0)));
    imposibleButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::selecImposible, this));
    _sheet->addChild(imposibleButton);

    /* Botón Volver Menu */
    CEGUI::Window* menuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "MenuButton");
    menuButton->setText("[font='Funkrocker-12']Volver");
    menuButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::exitDificultad, this));
    _sheet->addChild(menuButton);
}

/* Métodos para la eliminación de la GUI para el cambio de estado */
void MyFrameListener::deleteGUIMenu () {
    _sheet->destroyChild("NombreJuego");

    _sheet->destroyChild("PlayButton");
    _sheet->destroyChild("DificultadButton");
    _sheet->destroyChild("RecordsButton");
    _sheet->destroyChild("HowToButton");
    _sheet->destroyChild("CreditosButton");
    _sheet->destroyChild("SalirButton");
}

void MyFrameListener::deleteGUIGame () {
    _sheet->destroyChild("PararButton");
    _sheet->destroyChild("SalirButton");
    _sheet->destroyChild("MostrarButton");
    _sheet->destroyChild("OcultarButton");
}

void MyFrameListener::deleteGUIPause () {
    _sheet->destroyChild("ReanudarButton");
    _sheet->destroyChild("SalirButton");
}

void MyFrameListener::deleteGUIResultado () {
    _sheet->destroyChild("Resultado");
    _sheet->destroyChild("PlayAgainButton");
    _sheet->destroyChild("NotPlayAgainButton");
}

void MyFrameListener::deleteGUIRecords () {
    _sheet->destroyChild("RecordsLabel");
    _sheet->destroyChild("MenuButton");
}

void MyFrameListener::deleteGUICreditos () {
    _sheet->destroyChild("MenuButton");
    _sheet->destroyChild("Creditos");
}

void MyFrameListener::deleteGUIHowToPlay () {
    _sheet->destroyChild("MenuButton");
    _sheet->destroyChild("HowTo");
}

void MyFrameListener::deleteGUIDificultad () {
    _sheet->destroyChild("FacilButton");
    //_sheet->destroyChild("NormalButton");
    _sheet->destroyChild("ExpertoButton");
    _sheet->destroyChild("MenuButton");
}

void MyFrameListener::initGame () {
    inicializarTableros();
    inicializarTurno();
    dibujarTableros();

    /* IA */
    _sent = -1;
    _tocadaAnterior = false;
    _reves = false;
    _posicionesIA.clear();
    iniciarVectorPosicionesIA();
}

void MyFrameListener::inicializarTableros () {
    _tableroJugador = new Tablero();
    _tableroIA = new Tablero();

    _tableroJugador->colocarBarcos();
    _tableroIA->colocarBarcos();

    cout << "Tablero Jugador" << endl;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            Casilla* cas = _tableroJugador->getCasilla(i, j);
            if (cas->getBarco() == NULL) {
                cout << 0 << "  ";
            } else {
                cout << 1 << "  ";
            }
        }
        cout << endl;
    }

    cout << "\nTablero IA" << endl;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            Casilla* cas = _tableroIA->getCasilla(i, j);
            if (cas->getBarco() == NULL) {
                cout << 0 << "  ";
            } else {
                cout << 1 << "  ";
            }
        }
        cout << endl;
    }
}

void MyFrameListener::inicializarTurno () {
    if (_dificultadIA == IA_EXPERTO) {
        _turno = TURNO_JUGADOR;
    } else {
        int t = (int) (rand() % 2);

        switch (t) {
            case 0:
                _turno = TURNO_JUGADOR;
                break;
            case 1:
                _turno = TURNO_IA;
                break;
        }
    }
}

void MyFrameListener::dibujarTableros () {
    /* Se crea el nodo de Juego y se añade al Root */
    Ogre::SceneNode* nodeJuego = _sceneManager->createSceneNode("Juego");
    _sceneManager->getRootSceneNode()->addChild(nodeJuego);

    /* Se crean los nodos de los tableros */
    Ogre::SceneNode* nodeTableroIA = _sceneManager->createSceneNode("TableroIA");
    nodeTableroIA->pitch(Degree(85));
    nodeTableroIA->translate(Vector3(0, 4, 0));
    Ogre::SceneNode* nodeTableroJugador = _sceneManager->createSceneNode("TableroJugador");
    nodeTableroJugador->pitch(Degree(15));
    nodeTableroJugador->translate(Vector3(0, -3.5, 5));

    nodeJuego->addChild(nodeTableroIA);
    nodeJuego->addChild(nodeTableroJugador);

    /* Se añaden las entidades a los tableros IA y Jugador */
    Entity* entidadTableroIA = _sceneManager->createEntity("TableroIA", "Tablero.mesh");
    Entity* entidadTableroJugador = _sceneManager->createEntity("TableroJugador", "Tablero.mesh");

    nodeTableroIA->attachObject(entidadTableroIA);
    nodeTableroJugador->attachObject(entidadTableroJugador);

    /* Cajas para colisiones del tablero de IA */
    stringstream sauxnode, sauxmesh;
    string s = "Col_";
    for (int i = 1; i <= 10; i++) {
        for (int j = 1; j <= 10; j++) {
            sauxnode << s << i << "_" << j;
            sauxmesh << s << i << "_" << j << ".mesh";

            SceneNode *nodeaux = _sceneManager->createSceneNode(sauxnode.str());
            Entity *entaux = _sceneManager->createEntity(sauxnode.str(), sauxmesh.str());
            entaux->setQueryFlags(CUBO_TABLERO);

            nodeaux->attachObject(entaux);

            /* Ajustar bien las cajas de colision al tablero desde la vista de la camara */
            nodeaux->translate(Vector3((j <= 5 ? ((5 - j) * 0.02 + ((10 - i) * 0.01)) : (-((j - 5) * 0.02 + ((10 - i) * 0.01)))),
                    ((10 - i) * 0.05),
                    (i <= 5 ? ((6 - i) * 0.05) : 0 )));
            nodeaux->setVisible(false);
            nodeTableroIA->addChild(nodeaux);

            sauxnode.str("");
            sauxmesh.str("");
        }
    }

    /* Barcos para el tablero del jugador */
    Ship* barcosJugador = _tableroJugador->getBarcos();
    for (int i = 0; i < NUM_BARCOS; i++) {
        sauxnode << "Barco_" << i;
        Ship* barcoAux = &barcosJugador[i];

        int f = barcoAux->getFilaInicio();
        int c = barcoAux->getColumnaInicio();
        float desviof = 0;
        float desvioc = 0;
        float translatez = 0;

        int len = barcoAux->getLongitud();
        switch (len) {
            case 2:
                sauxmesh.str("BarcoPequeno.mesh");
                translatez = 0.5;
                break;
            case 3:
                sauxmesh.str("BarcoMediano.mesh");
                break;
            case 4:
                sauxmesh.str("BarcoGrande.mesh");
                break;
        }

        SceneNode *nodeaux = _sceneManager->createSceneNode(sauxnode.str());
        Entity *entaux = _sceneManager->createEntity(sauxnode.str(), sauxmesh.str());

        /* Se rota el barco al sentido que este tiene */
        switch (barcoAux->getSentido()) {
            case SENTIDO_ESTE:
                if (len == 3) {
                    desviof = 1;
                }
                if (len == 4) {
                    desviof = 1;
                }
                break;
            case SENTIDO_OESTE:
                if (len == 3) {
                    desviof = 0.1;
                }
                if (len == 4) {
                    desviof = 1;
                }
                nodeaux->yaw(Degree(180));
                break;
            case SENTIDO_NORTE:
                if (len == 3) {
                    desvioc = 0.35;
                }
                nodeaux->yaw(Degree(90));
                break;
            case SENTIDO_SUR:
                if (len == 3) {
                    desvioc = -0.35;
                }
                nodeaux->yaw(Degree(270));
                break;
        }
        nodeaux->translate(Vector3(0, 1 + translatez, 0));

        if (barcoAux->getSentido() == SENTIDO_ESTE || barcoAux->getSentido() == SENTIDO_OESTE) {
            nodeaux->translate(Vector3(0, 0,
                (f < 5) ? (desviof + (-(4.4 - f)) + (f * 0.05)) : (desviof + (f - 5) + 0.3 + (9 - f) * 0.1)));
        } else if (barcoAux->getSentido() == SENTIDO_NORTE || barcoAux->getSentido() == SENTIDO_SUR) {
            nodeaux->translate(Vector3((c < 5) ? (desvioc + (-(4.4 - c))) : (desvioc + (c - 4.5)),
                0, 0));
        }
        if (len == 2) {
            if (barcoAux->getSentido() == SENTIDO_ESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(-4 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 4, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_OESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(-5 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 5, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_NORTE) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 5 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 5));
                }
            } else if (barcoAux->getSentido() == SENTIDO_SUR) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 4 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 4));
                }
            }
        } else if (len == 3) {
            if (barcoAux->getSentido() == SENTIDO_ESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(-3.5 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 3.5, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_OESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(-5.5 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 5.5, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_NORTE) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 5 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 5));
                }
            } else if (barcoAux->getSentido() == SENTIDO_SUR) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 3 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 3));
                }
            }
        } else if (len == 4) {
            //nodeaux->scale(Vector3(0.95, 0.95, 0.95));
            if (barcoAux->getSentido() == SENTIDO_ESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(- 3 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 3, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_OESTE) {
                if (c < 5) {
                    nodeaux->translate(Vector3(- 6 + c, 0, 0));
                } else {
                    nodeaux->translate(Vector3(c - 6, 0, 0));
                }
            } else if (barcoAux->getSentido() == SENTIDO_NORTE) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 5 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 5));
                }
            } else if (barcoAux->getSentido() == SENTIDO_SUR) {
                if (f < 5) {
                    nodeaux->translate(Vector3(0, 0, - 2 + f));
                } else {
                    nodeaux->translate(Vector3(0, 0, f - 2));
                }
            }
        }

        nodeaux->attachObject(entaux);

        nodeTableroJugador->addChild(nodeaux);

        sauxnode.str("");
        sauxmesh.str("");
    }
}

void MyFrameListener::iniciarVectorPosicionesIA () {
    stringstream pos;

    if (_dificultadIA == IA_FACIL || _dificultadIA == IA_NORMAL) {
        for (int i = 0; i < DIMENSION_TABLERO; i++) {
            for (int j = 0; j < DIMENSION_TABLERO; j++) {
                pos << i << "_" << j;
                _posicionesIA.push_back(pos.str());
                pos.str("");
            }
        }
    } else if (_dificultadIA == IA_EXPERTO) {
        Ship* barcos = _tableroJugador->getBarcos();
        Ship* barcoAux;

        for (int i = 0; i < 5; i++) {
            barcoAux = &barcos[i];

            int x = barcoAux->getFilaInicio();
            int y = barcoAux->getColumnaInicio();
            int longitud = barcoAux->getLongitud();
            short sentido = barcoAux->getSentido();

            switch (sentido) {
                case SENTIDO_ESTE:
                    for (int j = 0; j < longitud; j++) {
                        pos << x << "_" << (y + j);
                        _posicionesIA.push_back(pos.str());
                        pos.str("");
                    }
                    break;
                case SENTIDO_OESTE:
                    for (int j = 0; j < longitud; j++) {
                        pos << x << "_" << (y - j);
                        _posicionesIA.push_back(pos.str());
                        pos.str("");
                    }
                    break;
                case SENTIDO_NORTE:
                    for (int j = 0; j < longitud; j++) {
                        pos << (x - j) << "_" << y;
                        _posicionesIA.push_back(pos.str());
                        pos.str("");
                    }
                    break;
                case SENTIDO_SUR:
                    for (int j = 0; j < longitud; j++) {
                        pos << (x + j) << "_" << y;
                        _posicionesIA.push_back(pos.str());
                        pos.str("");
                    }
                    break;
            }

            pos.str("");
        }
    }

    if (DEBUG_MODE) {
        for (uint i = 0; i < _posicionesIA.size(); i++) {
            cout << _posicionesIA[i] << ", ";
        }
        cout << endl;
    }
}

void MyFrameListener::limpiarEscena () {
    borrarTableros();
}

void MyFrameListener::borrarTableros () {
    /* Se eliminan las Entidades y acto seguido se borra el nodo Juego */
    stringstream sauxnode;
    string s = "Col_";
    for (int i = 1; i <= 10; i++) {
        for (int j = 1; j <= 10; j++) {
            sauxnode << s << i << "_" << j;

            _sceneManager->destroyEntity(sauxnode.str());

            sauxnode.str("");
        }
    }

    _sceneManager->destroyEntity("Barco_0");
    _sceneManager->destroyEntity("Barco_1");
    _sceneManager->destroyEntity("Barco_2");
    _sceneManager->destroyEntity("Barco_3");
    _sceneManager->destroyEntity("Barco_4");

    _sceneManager->destroyEntity("TableroJugador");
    _sceneManager->destroyEntity("TableroIA");

    _sceneManager->getRootSceneNode()->removeAndDestroyChild("Juego");
}

/* Código obtenido de la red. */
std::vector<string> MyFrameListener::split (string str, char delimiter) {
    std::vector<string> internal;
    stringstream ss(str);
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
}

void MyFrameListener::ejecutarTurnoIA () {
    int pos;
    std::vector<string> posV;

    _aleatorio = false;
    int f = -1;
    int c = -1;
    Casilla* cas;

    switch (_dificultadIA) {
        case IA_FACIL:
            pos = (int) (rand() % _posicionesIA.size());
            posV = split(_posicionesIA[pos], '_');
            f = atoi(posV[0].c_str());
            c = atoi(posV[1].c_str());

            cas = _tableroJugador->getCasilla(f, c);
            cas->tocar();

            /* Se elimina el elemento */
            _posicionesIA.erase(_posicionesIA.begin() + pos);
            break;
        case IA_NORMAL:
            if (!_tocadaAnterior) {
                pos = (int) (rand() % _posicionesIA.size());
                posV = split(_posicionesIA[pos], '_');
                f = atoi(posV[0].c_str());
                c = atoi(posV[1].c_str());

                cas = _tableroJugador->getCasilla(f, c);
                cas->tocar();

                if (cas->getBarco() != NULL) {
                    _filaAnterior = f;
                    _columnaAnterior = c;
                    _filaInicialBarco = f;
                    _columnaInicialBarco = c;
                    _tocadaAnterior = true;
                    _initAntes = false;
                }

                /* Se elimina el elemento */
                _posicionesIA.erase(_posicionesIA.begin() + pos);
                _reves = false;
            } else {
                /* Se inicializan los posibles sentidos del barco */
                if (!_initAntes) {
                    _posiblesSent.clear();
                    if (_columnaAnterior < (DIMENSION_TABLERO - 1)) {
                        cas = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior + 1);
                        if (!cas->getUsada()) {
                            _posiblesSent.push_back(SENTIDO_ESTE);
                        }
                    }
                    if (_columnaAnterior > 0) {
                        cas = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior - 1);
                        if (!cas->getUsada()) {
                            _posiblesSent.push_back(SENTIDO_OESTE);
                        }
                    }
                    if (_filaAnterior > 0) {
                        cas = _tableroJugador->getCasilla(_filaAnterior - 1, _columnaAnterior);
                        if (!cas->getUsada()) {
                            _posiblesSent.push_back(SENTIDO_NORTE);
                        }
                    }
                    if (_filaAnterior < (DIMENSION_TABLERO - 1)) {
                        cas = _tableroJugador->getCasilla(_filaAnterior + 1, _columnaAnterior);
                        if (!cas->getUsada()) {
                            _posiblesSent.push_back(SENTIDO_SUR);
                        }
                    }
                    _initAntes = true;
                }

                /* Si no quedan opciones de sentidos para una casilla */
                if (_posiblesSent.size() == 0) {
                    pos = (int) (rand() % _posicionesIA.size());
                    posV = split(_posicionesIA[pos], '_');
                    f = atoi(posV[0].c_str());
                    c = atoi(posV[1].c_str());

                    cas = _tableroJugador->getCasilla(f, c);
                    cas->tocar();

                    if (cas->getBarco() != NULL) {
                        _filaAnterior = f;
                        _columnaAnterior = c;
                        _filaInicialBarco = f;
                        _columnaInicialBarco = c;
                        _tocadaAnterior = true;
                        _initAntes = false;
                    }

                    /* Se elimina el elemento */
                    _posicionesIA.erase(_posicionesIA.begin() + pos);
                    _reves = false;
                } else { /* Si quedan celdas adyacentes */
                    /* Si no se tiene todavia el sentido */
                    if (_sent == -1) {
                        int randomPos = (int) (rand() % _posiblesSent.size());
                        _sent = _posiblesSent[randomPos];
                        _posiblesSent.erase(_posiblesSent.begin() + randomPos);

                        switch (_sent) {
                            case SENTIDO_ESTE:
                                f = _filaAnterior;
                                c = _columnaAnterior + 1;
                                _sent = SENTIDO_ESTE;
                                break;
                            case SENTIDO_OESTE:
                                f = _filaAnterior;
                                c = _columnaAnterior - 1;
                                _sent = SENTIDO_OESTE;
                                break;
                            case SENTIDO_NORTE:
                                f = _filaAnterior - 1;
                                c = _columnaAnterior;
                                _sent = SENTIDO_NORTE;
                                break;
                            case SENTIDO_SUR:
                                f = _filaAnterior + 1;
                                c = _columnaAnterior;
                                _sent = SENTIDO_SUR;
                                break;
                        }

                        cas = _tableroJugador->getCasilla(f, c);
                        cas->tocar();

                        /* Si hay barco en esa casilla se actualiza las posiciones */
                        if (cas->getBarco() != NULL) {
                            _filaAnterior = f;
                            _columnaAnterior = c;
                            _reves = false;
                        } else {
                            _sent = -1;
                        }
                    } else { /* Si se ha encontrado una dirección para el barco */
                        _aleatorio = false;
                        _vacia = false;
                        switch (_sent) {
                            case SENTIDO_ESTE:
                                if (!_reves) {
                                    if ((_columnaAnterior + 1) <= (DIMENSION_TABLERO - 1)) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior + 1);
                                        if (casAux->getUsada()) {
                                            _reves = true;
                                        } else {
                                            f = _filaAnterior;
                                            c = _columnaAnterior + 1;
                                            _columnaAnterior = c;
                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _reves = true;
                                    }
                                }
                                if (_reves) {
                                    _columnaAnterior = _columnaInicialBarco;
                                    _filaAnterior = _filaInicialBarco;

                                    if ((_columnaAnterior - 1) >= 0) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior - 1);
                                        if (casAux->getUsada()) {
                                            _aleatorio = true;
                                        } else {
                                            f = _filaAnterior;
                                            c = _columnaAnterior - 1;
                                            _columnaAnterior = c;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _aleatorio = true;
                                    }
                                    _columnaInicialBarco = c;
                                    _filaInicialBarco = f;
                                }
                                if (_vacia && !_reves) {
                                    _reves = true;
                                } else if (_vacia && _reves) {
                                    _tocadaAnterior = false;
                                }
                                break;
                            case SENTIDO_OESTE:
                                if (!_reves) {
                                    if ((_columnaAnterior - 1) >= 0) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior - 1);
                                        if (casAux->getUsada()) {
                                            _reves = true;
                                        } else {
                                            f = _filaAnterior;
                                            c = _columnaAnterior - 1;
                                            _columnaAnterior = c;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _reves = true;
                                    }
                                }
                                if (_reves) {
                                    _columnaAnterior = _columnaInicialBarco;
                                    _filaAnterior = _filaInicialBarco;

                                    if ((_columnaAnterior + 1) <= (DIMENSION_TABLERO - 1)) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior, _columnaAnterior + 1);
                                        if (casAux->getUsada()) {
                                            _aleatorio = true;
                                        } else {
                                            f = _filaAnterior;
                                            c = _columnaAnterior + 1;
                                            _columnaAnterior = c;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _aleatorio = true;
                                    }
                                    _columnaInicialBarco = c;
                                    _filaInicialBarco = f;
                                }
                                if (_vacia && !_reves) {
                                    _reves = true;
                                } else if (_vacia && _reves) {
                                    _tocadaAnterior = false;
                                }
                                break;
                            case SENTIDO_NORTE:

                                if (!_reves) {
                                    if ((_filaAnterior - 1) >= 0) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior - 1, _columnaAnterior);
                                        if (casAux->getUsada()) {
                                            _reves = true;
                                        } else {
                                            f = _filaAnterior - 1;
                                            c = _columnaAnterior;
                                            _filaAnterior = f;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _reves = true;
                                    }
                                }
                                if (_reves) {
                                    _columnaAnterior = _columnaInicialBarco;
                                    _filaAnterior = _filaInicialBarco;

                                    if ((_filaAnterior + 1) <= (DIMENSION_TABLERO - 1)) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior + 1, _columnaAnterior);
                                        if (casAux->getUsada()) {
                                            _aleatorio = true;
                                        } else {
                                            f = _filaAnterior + 1;
                                            c = _columnaAnterior;
                                            _filaAnterior = f;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _aleatorio = true;
                                    }
                                    _columnaInicialBarco = c;
                                    _filaInicialBarco = f;
                                }
                                if (_vacia && !_reves) {
                                    _reves = true;
                                } else if (_vacia && _reves) {
                                    _tocadaAnterior = false;
                                }
                                break;
                            case SENTIDO_SUR:
                                if (!_reves) {
                                    if ((_filaAnterior + 1) <= (DIMENSION_TABLERO - 1)) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior + 1, _columnaAnterior);
                                        if (casAux->getUsada()) {
                                            _reves = true;
                                        } else {
                                            f = _filaAnterior + 1;
                                            c = _columnaAnterior;
                                            _filaAnterior = f;
                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _reves = true;
                                    }
                                }
                                if (_reves) {
                                    _columnaAnterior = _columnaInicialBarco;
                                    _filaAnterior = _filaInicialBarco;

                                    if ((_filaAnterior - 1) >= 0) {
                                        Casilla* casAux = _tableroJugador->getCasilla(_filaAnterior - 1, _columnaAnterior);
                                        if (casAux->getUsada()) {
                                            _aleatorio = true;
                                        } else {
                                            f = _filaAnterior - 1;
                                            c = _columnaAnterior;
                                            _filaAnterior = f;

                                            if (casAux->getBarco() == NULL) {
                                                _vacia = true;
                                            }
                                        }
                                    } else {
                                        _aleatorio = true;
                                    }
                                    _columnaInicialBarco = c;
                                    _filaInicialBarco = f;
                                }
                                if (_vacia && !_reves) {
                                    _reves = true;
                                } else if (_vacia && _reves) {
                                    _tocadaAnterior = false;
                                }
                                break;
                        }

                        if (_aleatorio) {
                            pos = (int) (rand() % _posicionesIA.size());
                            posV = split(_posicionesIA[pos], '_');
                            f = atoi(posV[0].c_str());
                            c = atoi(posV[1].c_str());

                            cas = _tableroJugador->getCasilla(f, c);
                            cas->tocar();

                            if (cas->getBarco() != NULL) {
                                _filaAnterior = f;
                                _columnaAnterior = c;
                                _filaInicialBarco = f;
                                _columnaInicialBarco = c;
                                _tocadaAnterior = true;
                                _initAntes = false;
                            }

                            /* Se elimina el elemento */
                            _posicionesIA.erase(_posicionesIA.begin() + pos);
                        } else {
                            stringstream nCasilla;
                            nCasilla.str("");
                            nCasilla << f << "_" << c;
                            int posicion = find (_posicionesIA.begin(), _posicionesIA.end(), nCasilla.str()) - _posicionesIA.begin();
                            _posicionesIA.erase(_posicionesIA.begin() + posicion);
                        }
                    }
                }
            }
            break;
        case IA_EXPERTO:
            /* Se sigue el orden del vector debido a que tiene todas las posiciones
             * que contienen los barcos del jugador. */
            posV = split(_posicionesIA[0], '_');
            f = atoi(posV[0].c_str());
            c = atoi(posV[1].c_str());

            cas = _tableroJugador->getCasilla(f, c);
            cas->tocar();

            /* Se elimina el elemento */
            _posicionesIA.erase(_posicionesIA.begin());
            break;
    }

    /* Mostrar por pantalla la x de tu tablero */
    SceneNode *aux = _sceneManager->getSceneNode("TableroJugador");

    stringstream sauxnode;
    sauxnode.str("");
    sauxnode << "CruzIA_" << f << "_" << c;
    SceneNode *nodoCruzAux = _sceneManager->createSceneNode(sauxnode.str());

    Ship* barco = cas->getBarco();
    Entity *entCruzAux;
    if (barco != NULL) {
        entCruzAux = _sceneManager->createEntity("CruzRoja.mesh");
    } else {
        entCruzAux = _sceneManager->createEntity("CruzBlanca.mesh");
    }
    nodoCruzAux->attachObject(entCruzAux);

    aux->addChild(nodoCruzAux);

    nodoCruzAux->scale(Vector3(0.8, 0.8, 0.8));

    float desp_f = - 4.2 + f;
    float desp_c = 0;
    float desp_front = 0.5;

    if (c < 5) {
        desp_c = - 4.5 + c;
    } else {
        desp_c = c - 4.5;
    }

    nodoCruzAux->translate(Vector3(desp_c, desp_front, desp_f));
    nodoCruzAux->pitch(Degree(15));
}

bool MyFrameListener::comprobarGanar (int t) {
    int n = NUM_BARCOS;

    Ship* s;

    for (int i = 0; i < NUM_BARCOS; i++) {
        if (t == TURNO_JUGADOR) {
            s = &_tableroIA->getBarcos()[i];
        } else if (t == TURNO_IA) {
            s = &_tableroJugador->getBarcos()[i];
        }

        if (s->getHundido()) {
            n--;
        }
    }

    if (n == 0)
        return true;
    else
        return false;
}

void MyFrameListener::createRecordsFile () {
    fstream recordsFile;
    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);
    if (!recordsFile.good()) {
        recordsFile.close();
        recordsFile.open("records.rcd", ios::out | ios::trunc | ios::binary);
        for (int i = 0; i < 5; i++) {
            recordsFile << 100 << "\n";
        }
        recordsFile.close();
    } else {
        recordsFile.close();
    }
}

void MyFrameListener::saveRecords () {
    fstream recordsFile;
    string line;
    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);

    if (recordsFile.good()) {
        int pos = 0;
        while (getline(recordsFile, line)) {
			int v = atoi(line.c_str());
			if (_movimientos > v) {
				pos++;
			}
		}
        recordsFile.close();

    	recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);
        for (int i = 0; i < pos; i++) {
			getline(recordsFile, line);
		}
        streampos posaux = recordsFile.tellg();
        getline(recordsFile, line);
        recordsFile.seekg(posaux);
        cout << line << endl;
        recordsFile << _movimientos << endl << line << endl;
        recordsFile.close();
    } else {
        recordsFile.close();
    }
}

string MyFrameListener::createStringRecords () {
    stringstream res;
    fstream recordsFile;
    string line;

    res.str("");
    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);
    if (recordsFile.good()) {
        int i = 1;
        while (getline(recordsFile, line)) {
            if (line.size() > 0 && atoi(line.c_str()) > 14 && i <= 5) {
                int movimientos = atoi(line.c_str());
                res << i << " " << movimientos << "\n";
                i++;
            }
        }
    }
    recordsFile.close();
    return res.str();
}
