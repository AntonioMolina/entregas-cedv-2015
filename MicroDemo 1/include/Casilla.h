#ifndef CASILLA_H
#define CASILLA_H

#include "Ship.h"

class Casilla {
    public:
        Casilla ();
        Casilla (Ship* barco);
        ~Casilla ();

        bool getUsada ();
        Ship* getBarco ();

        void setUsada (bool usada);
        void setBarco (Ship* barco);

        void tocar ();

    private:
        bool _usada;
        Ship* _barco;

};

#endif
