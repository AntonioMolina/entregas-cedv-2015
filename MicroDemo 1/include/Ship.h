#ifndef SHIP_H
#define  SHIP_H

class Ship {
    public:
        Ship (int longitud);
        Ship ();
        ~Ship ();

        int getLongitud () const;
        int getContador () const;
        bool getHundido () const;
        short getSentido () const;
        int getFilaInicio () const;
        int getColumnaInicio () const;

        void setLongitud (int longitud);
        void setHundido (bool hundido);
        void setSentido (short sentido);
        void setFilaInicio (int filaInicio);
        void setColumnaInicio (int columnaInicio);

        void tocarBarco ();

    private:
        short _sentido;
        int _longitud;
        int _contador;
        bool _hundido;

        int _filaInicio;
        int _columnaInicio;

};

#endif /* SHIP_H */
