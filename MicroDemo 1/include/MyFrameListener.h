#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

#include <math.h>

#include <Ogre.h>
#include <OgreNode.h>
#include <OgreSceneQuery.h>
#include <OIS.h>
#include <CEGUI.h>

#include <RendererModules/Ogre/Renderer.h>

#include "Constantes.h"
#include "Tablero.h"

using namespace Ogre;
using namespace std;

class MyFrameListener : public FrameListener, OIS::KeyListener, OIS::MouseListener {
    public:
        MyFrameListener (RenderWindow* window, Camera* camera,
                    SceneManager* sceneManager);
        ~MyFrameListener ();

        bool frameStarted (const FrameEvent& evt);

        /* Creación y borrado del fondo */
        void createBackGround ();
        void deleteBackGround ();

        /* Métodos para creación de GUI para los distintos estados */
        void setCEGUI ();
        void createGUIMenu ();
        void createGUIGame ();
        void createGUIPause ();
        void createGUIResultado ();
        void createGUIRecords ();
        void createGUICreditos ();
        void createGUIHowToPlay ();
        void createGUIDificultad ();

        /* Métodos para la eliminación de la GUI para el cambio de estado */
        void deleteGUIMenu ();
        void deleteGUIGame ();
        void deleteGUIPause ();
        void deleteGUIResultado ();
        void deleteGUIRecords ();
        void deleteGUICreditos ();
        void deleteGUIHowToPlay ();
        void deleteGUIDificultad ();

        /* Métodos para gestión de CEGUI */
        /* Menu */
        bool startGame (const CEGUI::EventArgs &e);
        bool exitGame (const CEGUI::EventArgs &e);
        bool recordsGame (const CEGUI::EventArgs &e);
        bool howtoGame (const CEGUI::EventArgs &e);
        bool creditosGame (const CEGUI::EventArgs &e);
        bool selectDificultad (const CEGUI::EventArgs &e);

        /* Juego */
        bool pauseMatch (const CEGUI::EventArgs &e);
        bool exitMatch (const CEGUI::EventArgs &e);
        bool ocultarBarcos (const CEGUI::EventArgs &e);
        bool mostrarBarcos (const CEGUI::EventArgs &e);

        /* Pausa */
        bool resumeMatch (const CEGUI::EventArgs &e);
        bool finishMatch (const CEGUI::EventArgs &e);

        /* Resultados */
        bool gameAgain (const CEGUI::EventArgs &e);
        bool notPlayAgain (const CEGUI::EventArgs &e);

        /* Como jugar */
        bool howToMenu (const CEGUI::EventArgs &e);

        /* Records */
        bool exitRecords (const CEGUI::EventArgs &e);

        /* Creditos */
        bool exitCreditos (const CEGUI::EventArgs &e);

        /* Dificultad */
        bool selecFacil (const CEGUI::EventArgs &e);
        bool selecNormal (const CEGUI::EventArgs &e);
        bool selecImposible (const CEGUI::EventArgs &e);
        bool exitDificultad (const CEGUI::EventArgs &e);

    private:
        float _timeSinceLastFrame;
        int _estado;
        int _turno;
        int _ganador;

        int _movimientos;

        /* Variables usadas para la IA */
        int _dificultadIA;
        int _filaAnterior, _columnaAnterior;
        int _filaInicialBarco, _columnaInicialBarco;
        int _sent;
        bool _reves, _aleatorio, _vacia;
        bool _tocadaAnterior, _initAntes;
        std::vector<int> _posiblesSent;
        std::vector<string> _posicionesIA;

        OIS::InputManager* _inputManager;
        OIS::Keyboard* _keyboard;
        OIS::Mouse* _mouse;

        RenderWindow* _window;
        Camera* _camera;

        SceneManager* _sceneManager;
        SceneNode* _selectedNode;
        RaySceneQuery* _raySceneQuery;

        CEGUI::Window* _sheet;
        CEGUI::OgreRenderer* _renderer;

        Tablero* _tableroJugador;
        Tablero* _tableroIA;
        Ship* _barcosJugador;
        Ship* _barcosIA;

        bool keyPressed (const OIS::KeyEvent& evt);
        bool keyReleased (const OIS::KeyEvent& evt);

        bool mouseMoved (const OIS::MouseEvent& evt);
        bool mousePressed (const OIS::MouseEvent& evt, OIS::MouseButtonID id);
        bool mouseReleased (const OIS::MouseEvent& evt, OIS::MouseButtonID id);

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);

        Ray setRayQuery (int posx, int posy, uint32 mask);

        void initGame ();
        void inicializarTableros ();
        void inicializarTurno ();
        void dibujarTableros ();
        void iniciarVectorPosicionesIA ();
        void borrarTableros ();

        void limpiarEscena ();

        std::vector<string> split (string str, char delimiter);
        void ejecutarTurnoIA ();

        bool comprobarGanar (int t);

        void createRecordsFile ();
        void saveRecords ();
        string createStringRecords ();
};
