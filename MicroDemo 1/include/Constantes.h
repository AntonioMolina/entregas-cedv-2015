#ifndef CONSTANTES_H
#define CONSTANTES_H

#define DEBUG_MODE false

#define UNUSED_VARIABLE(x) (void)x

#define ESTADO_MENU      0
#define ESTADO_CARGA     1
#define ESTADO_JUEGO     2
#define ESTADO_PAUSA     3
#define ESTADO_RESULTADO 4
#define ESTADO_RECORDS   5
#define ESTADO_CREDITOS  6
#define ESTADO_HOWTO     7
#define ESTADO_SELEC_IA  8
#define ESTADO_SALIR     9

#define DIMENSION_TABLERO 10

#define NUM_BARCOS 5

#define SENTIDO_ESTE  0
#define SENTIDO_NORTE 1
#define SENTIDO_OESTE 2
#define SENTIDO_SUR   3

#define TURNO_JUGADOR 0
#define TURNO_IA      1

#define IA_FACIL   0
#define IA_NORMAL  1
#define IA_EXPERTO 2

#define GANADOR_JUGADOR 0
#define GANADOR_IA      1

#define CUBO_TABLERO 1 << 0

#endif /* CONSTANTES_H */
