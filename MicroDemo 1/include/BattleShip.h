#include <time.h>

#include <Ogre.h>
#include <RendererModules/Ogre/Renderer.h>

#include "MyFrameListener.h"

class BattleShip {
    public:
        BattleShip ();
        ~BattleShip ();

        int start ();
        void loadResources ();
        void createScene ();

    private:
        Ogre::SceneManager* _sceneManager;
        Ogre::Root* _root;

        MyFrameListener* _framelistener;
};
