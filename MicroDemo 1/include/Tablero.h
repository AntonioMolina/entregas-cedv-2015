#ifndef TABLERO_H
#define TABLERO_H

#include <cstdlib>

#include <Ogre.h>

#include "Casilla.h"
#include "Constantes.h"

using namespace std;

class Tablero {
    public:
        Tablero ();
        ~Tablero ();

        Ship* getBarcos ();

        void inicializarBarcos ();
        void colocarBarcos ();
        Casilla* getCasilla (int x, int y);
        //bool tocarCasilla (int x, int y);

    private:
        Casilla** _tablero;
        Ship* _barcos;
};

#endif /* TABLERO_H */
